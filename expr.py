from abc import ABCMeta, abstractmethod
from yices import Terms

class Expr:
  __metaclass__ = ABCMeta

  @abstractmethod
  def toSMT(self, subst):
      pass

class Term(Expr):
  def __init__(self):
    pass

  def toSMT(self, subst):
    return Terms.true()

class Var(Term):
  def __init__(self, c, prime):
    #assert(isinstance(c, basestring))
    self.name = c
    self.is_prime = (prime != None)
  
  def __str__(self):
    return self.name + ("'" if self.is_prime else "")

  def toSMT(self, subst):
    return subst[str(self)]


class Num(Term):
  def __init__(self, c):
    self.value = c
  
  def __str__(self):
    return self.value

  def toSMT(self, subst):
    return Terms.parse_float(self.value)


class Charstr(Term):
  def __init__(self, c):
    self.value = c
  
  def __str__(self):
    return '"' + self.value + '"'

  def toSMT(self, subst):
    # FIXME represent strings better
    n = 0 if self.value == "NIL" else ord(self.value[0]) 
    return Terms.integer(n)


class BinOp(Term):

  def __init__(self, a, op, b):
    self.op = op
    assert(op == "+" or op == "-")
    self.left = a
    self.right = b
  
  def __str__(self):
    return "(" + str(self.left) + " " + self.op + " " +\
		       str(self.right) + ")"

  def toSMT(self, subst):
    op_funs = {
      "+"  : lambda a, b: Terms.add(a, b),
      "-" : lambda a, b: Terms.sub(a, b),
    }
    return op_funs[self.op](self.left.toSMT(subst), self.right.toSMT(subst))


class Cmp(Term):

  def __init__(self, op, a, b):
    self.op = op
    self.left = a
    self.right = b

  def __str__(self):
    return "(" + str(self.left) + " " + self.op + " " +\
		       str(self.right) + ")"

  def toSMT(self, subst):
    op_funs = {
      "=="  : lambda a, b: Terms.arith_eq_atom(a, b),
      ">=" : lambda a, b: Terms.arith_geq_atom(a, b),
      "<=" : lambda a, b: Terms.arith_leq_atom(a, b),
      ">"  : lambda a, b: Terms.arith_gt_atom(a, b),
      "<"  : lambda a, b: Terms.arith_lt_atom(a, b),
      "!=" : lambda a, b: Terms.arith_neq_atom(a, b),
    }
    return op_funs[self.op](self.left.toSMT(subst), self.right.toSMT(subst))

class BinCon(Term):

  def __init__(self, a, op, b):
    self.op = op
    assert(op == "&&" or op == "||")
    self.left = a
    self.right = b
  
  def __str__(self):
    return "(" + str(self.left) + " " + self.op + " " +\
		       str(self.right) + ")"

  def toSMT(self, subst):
    op_funs = {
      "&&"  : lambda a, b: Terms.yand([a, b]),
      "||" : lambda a, b: Terms.yor([a, b]),
    }
    return op_funs[self.op](self.left.toSMT(subst), self.right.toSMT(subst))
