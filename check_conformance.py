from yices import *
import sys
from functools import reduce
from collections import defaultdict
from itertools import groupby
from yread import read_json_input, read_pnml_input
import pm4py
import time

t_encode = 0.0
t_solve = 0.0

### printing
def spaces(n):
  return "" if n <= 0 else " " + spaces(n-1) 

def fill_to(s, n):
  return s + spaces(n - len(s)) if len(s) < n else s[:n]

def print_sequence(s, tab = 12):
  a = spaces(tab+1)
  for i in range(0, len(s)):
    a += fill_to(s[i]["label"], tab) + " "
  print(a)
  for v in dpn["variables"]:
    name = v["name"]
    a = fill_to(name, tab) + " "
    for i in range(0, len(s)):
      
      val = s[i]["valuation"]
      val_pre = None if i == 0 else s[i-1]["valuation"]
      nochange = (i==0 or (name not in val_pre) or (val[name]!=val_pre[name])) \
        if name in val else None
      if name in val and nochange:
        a += fill_to(str(val[name]), tab) + " "
      else:
        a += spaces(tab+1)
    print(a)

### add silent transitions to all final places (without label and constraint)
def add_silent_finals(dpn, map):
  id = len(map) + 1
  for p in dpn["places"]:
    if "final" in p:
      t = {"id": id, "invisible": True, "label":None }
      dpn["transitions"].append(t)
      dpn["arcs"].append({"source": p["id"], "target": id})
      dpn["arcs"].append({"target": p["id"], "source": id})
      map[id] = t
      id += 1
  return dpn, map

def mk_ids_integer(dpn):
  id = 0
  str2int = {}
  int2plc = {}
  for p in dpn["places"]:
    n = p["id"]
    int2plc[id] = dict(p) 
    p["id"] = id
    str2int[n] = id
    id += 1
  for p in dpn["transitions"]:
    n = p["id"]
    int2plc[id] = dict(p)
    p["id"] = id
    str2int[n] = id
    id += 1
  for a in dpn["arcs"]:
    a["source"] = str2int[a["source"]]
    a["target"] = str2int[a["target"]]
  return dpn, int2plc

def preprocess_trace(log, dpn):
  simple_log = []
  for e in log:
    valuation = {}
    for v in dpn["variables"]:
      if v["name"] in e:
        val = e[v["name"]]
        valuation[v["name"]] = val if not isinstance(val,str) else \
          0 if val == "NIL" else ord(val[0])
    simple_log.append({"label" : e["concept:name"], "valuation": valuation})
  return simple_log

# shortest path to final state
def shortest_accepted(net):
  arcs = net["arcs"]
  def shortest(n, ns):
    if "final" in n and n["final"]:
      return 0
    elif n in ns:
      return 1000 # the hack to infinity
    else:
      trans = [ l["target"] for l in arcs if l["source"] == n["id"] ]
      next_places = [ l["target"] for l in arcs if l["source"] in trans ]
      return 1 + min([ shortest(places[p], [n] + ns) for p in next_places ] + [1000])
  
  places = dict([ (p["id"], p) for p in net["places"] ])
  initials = [p for p in places.values() if "initial" in p and p["initial"]]
  return min([ shortest(p, []) for p in initials ])

### create variables
def intvar(name):
  int_t = Types.int_type()
  return Terms.new_uninterpreted_term(int_t)

def realvar(name):
  real_t = Types.real_type()
  return Terms.new_uninterpreted_term(real_t)

# dictionary of var name to variable for instant i
def data_vars(dpn, timeframe):
  vs = dpn["variables"]
  name = lambda v, i: "_" + v["name"] + str(i)
  dvs = lambda i: dict([ (v["name"], realvar(name(v,i))) for v in vs])
  return dict([ (i, dvs(i)) for i in timeframe ])

# variables for marking: 
# returns array vs mapping instant i to array vs[i] mapping place id to integer
# variable for number of tokens n, i.e. vs[i][place_id] = n
def marking_vars(dpn, timeframe):
  var = lambda i, id: intvar("marked_" + str(i) + "_" + str(id))
  mvars = lambda i: dict([(p["id"], var(i, p["id"])) for p in dpn["places"] ])
  return dict([(i, mvars(i)) for i in timeframe ])

# transition variables for instant: dictionary of transition ids to vars
def trans_vars(dpn, instant):
  prefix = "trans" + str(instant)
  return intvar(prefix)

def edit_distance_vars(n, m):
  zero = Terms.integer(0)
  def var(i, j):
    return intvar("d"+str(i)+"_"+str(j)) if i > 0 or j > 0 else zero
  
  return [[var(i,j) for j in range(0,m+1)] for i in range(0,n+1)]

### encoding parts
def yint(n):
  return Terms.integer(n)

def yand(l):
  return Terms.yand(l)

def yor(l):
  return Terms.yor(l)

def yeq(a, b):
  return Terms.arith_eq_atom(a, b)

def is_gt0(a):
  return Terms.arith_gt0_atom(a)

def is_lt(a, b):
  return Terms.arith_lt_atom(a, b)

def is_ge(a, b):
  return Terms.arith_geq_atom(a, b)

def inc(a):
  return Terms.add(a, yint(1))

def rat(n):
  return Terms.parse_float(str(n))

def minus(a, b):
  return Terms.sub(a, b)

def plus(a, b):
  return Terms.add(a, b)

def neg(a):
  return Terms.ynot(a)

def implies(a, b):
  return Terms.implies(a, b)

def ite(cond, a, b):
  return Terms.ite(cond, a, b)

# fix initial state and initial valuation
def initial_state(mvars, data_vars, dpn):
  mcount = [(p["id"], p["initial"] if "initial" in p else 0) \
    for p in dpn["places"]]
  init_marking = [yeq(mvars[0][p], yint(c)) for (p, c) in mcount]
  vars = dpn["variables"]
  init_val = [(v["name"], v["initial"] if "initial" in v else 0) for v in vars]
  fix_data = [ yeq(data_vars[0][x], yint(a)) for (x,a) in init_val ]
  return  yand(init_marking + fix_data)

# for every instant i the transition variable trans_vars[i] has as value 
# a transition id
def transition_range(trans_vars, dpn):
  oneof = lambda v: yor([yeq(v, yint(t["id"])) for t in dpn["transitions"]])
  return yand([oneof(v) for v in trans_vars])

# the last marking is the final marking
def final_state(mvars, steps, dpn):
  fmark = [(p["id"], p["final"] if "final" in p else 0) for p in dpn["places"]]
  return yand([yeq(mvars[steps][p], yint(cnt)) for (p, cnt) in fmark])

# encode a valid transition sequence
def token_constraints(t, i, mvars, dpn):
  pre = [ e["source"] for e in dpn["arcs"] if e["target"] == t["id"] ]
  post = [ e["target"] for e in dpn["arcs"] if e["source"] == t["id"] ]
  pre_not_post = [ p for p in pre if not p in post ]
  post_not_pre = [ p for p in post if not p in pre ]
  pd2 = dict(groupby(post)) # dictionary containing post with multiplicity
  pd1 = list(groupby(pre))
  pre_post = [ (p, len(list(l))-len(list(pd2[p]))) for (p,l) in pd1 if p in pd2]
  pids = [p["id"] for p in dpn["places"]]
  others =  [ p for p in pids if not (p in post or p in pre) ]
  pre_enabled = [ is_gt0(mvars[i][p]) for p in pre ]
  pre_change = [ yeq(mvars[i][p], inc(mvars[i+1][p])) \
    for p in pre_not_post ]
  post_chng = [ yeq(mvars[i+1][p],inc(mvars[i][p])) for p in post_not_pre]
  both_chng = [ yeq(minus(mvars[i][p], mvars[i+1][p]), yint(d)) \
    for (p,d) in pre_post ]
  stay_same = [ yeq(mvars[i][p], mvars[i+1][p]) for p in others ]
  return yand(pre_enabled + pre_change + post_chng + both_chng + stay_same)

def data_constraints(t, i, data_vars, dpn):
  vars = [v["name"] for v in dpn["variables"]]
  subst_prime = [ (x + "'", v) for (x, v) in data_vars[i+1].items() ]
  subst = dict(list(data_vars[i].items()) + subst_prime)
  is_constr = "constraint" in t
  trans_constr = t["constraint"].toSMT(subst) if is_constr else Terms.true()
  guard = t["guard"] if "guard" in t else ""
  written = lambda v: v+"'" in guard or "write" in t and v in t["write"] 
  keep = [v for v in vars if not written(v) ] if is_constr or "write" in t \
    else vars
  keep_constr = [yeq(data_vars[i][v], data_vars[i+1][v]) for v in keep]
  return yand([trans_constr] + keep_constr)

def transition_constraints(mvars, trans_vars, data_vars, dpn):
  # conditions on transition variable tvar for instant i having value tindex, 
  # which corresponds to transition t
  def step(tvar, i, trans):
    (tindex, t) = trans
    token_change = token_constraints(t, i, mvars, dpn)
    data_change = data_constraints(t, i, data_vars, dpn)
    return implies(yeq(tvar, yint(t["id"])), yand([token_change, data_change]))

  # big conjunction over all possible steps
  etrans_vars = list(enumerate(trans_vars))
  etrans = list(enumerate(dpn["transitions"]))
  return yand([ step(v, i, t) for (i,v) in etrans_vars for t in etrans ])

def edit_distance(log, delta, vs_trans, vs_data, dpn):
  # delta dimensions in notation of paper (n+1) x (m+1) 
  n = len(delta) - 1
  m = len(delta[0]) - 1 # length of log

  etrans = [(t["id"], t) for t in dpn["transitions"]]
  vars = dpn["variables"]
  
  def write(t):
    gw = [ v for v in vars if v+"'" in t["guard"]] if "guard" in t else []
    w = t["write"] if "write" in t else []
    if not set(gw).issubset(set(w)):
      print("mismatch in written variables for transition " + str(t["id"]))
    return len(w)

  def wcost(t):
    return 0 if t["invisible"] else 1 + write(t) # unless silent: written vars
  wcost = dict([ (t["id"], yint(wcost(t))) for t in dpn["transitions"]])

  def eq(i, j):
    subst_prime = dict([ (x, v) for (x, v) in vs_data[i+1].items() ])
    valuation = log[j]["valuation"].items() if "valuation" in log[j] else None

    def subst_diff(t):
      if not "valuation" in log[j]:
        return yint(0)
      s = yint(0)
      for (x, a) in valuation:
        s = ite(yeq(subst_prime[x], rat(a)), s, inc(s))
      return s

    return [ (yeq(vs_trans[i], yint(id)), subst_diff(t)) for (id,t) in etrans \
      if "label" in t and t["label"] == log[j]["label"] ]
  
  def neq(i, j):
    c = log[j]["label"]
    return [ (yeq(vs_trans[i], yint(id)), wcost[t["id"]]) for (id,t) in etrans \
      if not t["invisible"] and t["label"] != c ]
  
  # write costs for vs_trans[i], alternative over all transitions
  def wcosts(i):
    return reduce(lambda c,t: \
      ite(yeq(vs_trans[i], yint(t[0])), wcost[t[1]["id"]], c), etrans, yint(0))

  def silent(i): # transition i is silent
    return yor([ yeq(vs_trans[i], yint(id)) \
      for (id, t) in etrans if t["invisible"] ])
  silents = [ silent(i) for i in range(0,n) ]

  # delta[i][j] means edit distance of trans|_{<=i} and log|_{<=j}
  # base cases
  varrange = yand([is_ge(delta[i][j],  yint(0))\
     for i in range(0,n+1) for j in range(0,m+1)])
  # init = (delta[0][0] == 0) in var creation
  base1 = yand([ implies(neg(silents[i]), \
    yeq(delta[i+1][0], plus(delta[i][0], wcosts(i)))) for i in range(0,n)])
  base2 = yand([ yeq(delta[0][j+1], yint(j + 1))  for j in range(0,m) ])
  # step cases
  equal = yand([ implies(is_t, \
    yeq(delta[i+1][j+1], plus(penalty,  delta[i][j]))) \
     for i in range(0,n) for j in range(0,m) for (is_t, penalty) in eq(i, j)])
  neq_conj = []
  for i in range(0,n):
    for j in range(0,m):
      for (is_t, penalty) in neq(i, j):
        a = plus(penalty, delta[i][j+1])
        b = inc(delta[i+1][j])
        imp = implies(is_t, yeq(delta[i+1][j+1], ite(is_lt(a, b), a, b)))
        neq_conj.append(imp)
  silent = [ implies(silents[i], yeq(delta[i][j], delta[i+1][j])) \
    for i in range(0,n) for j in range(0,m+1) ]
  
  return (delta[n][m], yand([varrange, base1, base2, equal] + neq_conj + silent))

# main conformance check: combine encoding, check, decode
def conformance_check_trace(ctx, log, dpn, verbosity=1):
  global t_encode, t_solve
  # shortest path to final state 
  shortest_acc_path = shortest_accepted(dpn)
  # print("shortest path to accepted ", shortest_acc_path)
  # estimate of upper bound on steps to be considered
  # FIXME for more than 1 tokens this can be too tight
  timeframe = len(log) + shortest_acc_path
  placerange = range(0,timeframe+1)
  steprange = range(0,timeframe)

  # create vars
  vs_data = data_vars(dpn, placerange)
  vs_places = marking_vars(dpn, placerange)
  vs_trans = [ trans_vars(dpn, i) for i in steprange ]
  vs_edit_distance = edit_distance_vars(timeframe, len(log))

  # encoding parts
  t_start = time.perf_counter()
  f_initial = initial_state(vs_places, vs_data, dpn)
  f_trans_range = transition_range(vs_trans, dpn)
  f_tokens = transition_constraints(vs_places, vs_trans, vs_data, dpn)
  f_final = final_state(vs_places, timeframe, dpn)
  (dist, dconstr) = edit_distance(log, vs_edit_distance, vs_trans, vs_data, dpn)
  t_encode = time.perf_counter() - t_start

  # assert
  ctx.assert_formulas([f_initial, f_trans_range, f_tokens, f_final])
  ctx.assert_formulas([dconstr])

  # check + print model
  ctx.push()
  inc_distance = 0
  ctx.assert_formulas([yeq(dist, yint(inc_distance))])
  t_start = time.perf_counter()
  status = ctx.check_context()
  t_solve = time.perf_counter() - t_start
  while status != Status.SAT and inc_distance <= timeframe:
    ctx.pop()
    ctx.push()
    inc_distance += 1
    ctx.assert_formulas([yeq(dist, yint(inc_distance))])
    status = ctx.check_context()

  model = Model.from_context(ctx, 1)
  distance = model.get_value(dist)

  if verbosity > 0:
    valuations = []
    print("MARKING:")
    for i in placerange:
      valuation = {}
      for (x, var) in vs_data[i].items():
        f = model.get_value(var)
        valuation[x] = float(f)
      valuations.append(valuation)
      marking = ""
      for (p,count) in list(vs_places[i].items()):
        count_val = model.get_value(count)
        for c in range(0, count_val):
          marking = marking + (" " if marking else "") + str(map[p]["name"])
      print("%i: (%s)" % (i, marking))

    transs = dict([(t["id"], t) for t in dpn["transitions"]])
    label = lambda i: "tau" if not transs[i]["label"] else transs[i]["label"]
    modelseq = []
    for i in steprange:
      tid = model.get_value(vs_trans[i])
      l = label(model.get_value(vs_trans[i]))
      modelseq.append({"label": l, "valuation": valuations[i+1]})

    print("LOG SEQUENCE:")
    print_sequence(log)
    print("\nMODEL SEQUENCE:")
    print_sequence(modelseq)
    #for i in range(0, len(vs_edit_distance)):
    #  for j in range(0, len(vs_edit_distance[0])):
    #    print("delta[%d][%d] = %d" % (i, j, model.eval(vs_edit_distance[i][j]).as_long()))
  return distance

### main
if __name__ == "__main__":
  modelfile = sys.argv[1]
  logfile = sys.argv[2]
  dpn = read_pnml_input(modelfile)
  logs = pm4py.read_xes(logfile)

  # preprocessing
  dpn, map = mk_ids_integer(dpn)
  dpn, map = add_silent_finals(dpn, map)
  logs = [preprocess_trace(trace, dpn) for trace in logs]
  print("number of logs: %d" % len(logs))

  cfg = Config()
  cfg.default_config_for_logic('QF_LRA')
  ctx = Context(cfg)

  t_encode_total = 0
  t_solve_total = 0
  t_check_total = 0
  t_start = time.perf_counter()
  distances = {}
  for (i, trace) in enumerate(logs[:1000]):
    print("##### CONFORMANCE CHECK TRACE %d" % i)
    distance = conformance_check_trace(ctx, trace, dpn, verbosity=1)
    print("DISTANCE : " + str(distance))
    print("time/encode: %.2f  time/solve: %.2f" % (t_encode, t_solve))
    t_encode_total += t_encode
    t_solve_total += t_solve
    distances[distance] =distances[distance] + 1 if distance in distances else 1
    ctx.reset_context()

  cfg.dispose()
  ctx.dispose()
  Yices.exit()
  
  t_check_total += time.perf_counter() - t_start
  print("total time/encode: %.2f  time/solve: %.2f" % \
    (t_encode_total, t_solve_total ))
  for i in range(0, len(distances)):
    print("distance %d: %d" % (i, distances[i]))
