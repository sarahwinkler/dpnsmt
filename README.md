# dpnsmt
Conformance checking of DPNs by SMT.

## requirements
The script is written for python3, and requires currently the following:
 * yices 
 * pm4py
They can e.g. using pip:
 $ pip3 install yices

## example
 $ python3 check_conformance.py examples/sat_paper_fig2.pnml examples/sat_paper_fig2.xes

